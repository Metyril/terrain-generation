// Texturing
// Eric Galin

const float view = 5000.0;  // View distance
const float Epsilon = 0.05; // Marching epsilon

// Iterations
const int Steps = 256;      // Maximum number of steps when sphere-tracing
const float K = 7.0;	    // Lipschitz constant

// Rendering
const float fog = 5000.0;   // Fog attenuation distance

float ridgeNoise( in vec2 x )
{
	// Rotation matrix
	const mat2 m2 = mat2(0.8,-0.6,0.6,0.8);

    const float l0=1350.0;  // Initial wavelength
    const float a0=415.0;   // Initial amplitude
    
	float a = -300.0;       // Initial height
    float b = a0;           
	vec2  p = x/l0;
    for( int i=0; i<10; i++ )
    {
        float n = 1.0 - 2.0 * pow(abs(Noise(p) - 0.5), 1.0); //ridge noise ?
        //float n = 1.0 - pow(abs(Noise(p) - 0.5), 1.0); //ridge noise ?
        //float n = Noise(p);
        a += b*n;
		b *= 0.5;
        p = m2*p*2.0;
    }
	return a;
}

// Terrain heightfield
// x : Position in the plane
float Terrain( in vec2 x )
{
	// Rotation matrix
	const mat2 m2 = mat2(0.8,-0.6,0.6,0.8);

    const float l0=1350.0;  // Initial wavelength
    const float a0=415.0;   // Initial amplitude
    
	float a = -1000.0;       // Initial height
    float b = a0;           
	vec2  p = x/l0;
    for( int i=0; i<10; i++ )
    {
        //float n = 1.0 - 2.0 * pow(abs(Noise(p) - 0.5), 1.0); //ridge noise ?
        float n = 1.0 - pow(abs(Noise(p) - 0.5), 1.2); //ridge noise ?
        //float n = Noise(p);
        a += b*n;
		b *= 0.5;
        p = m2*p*2.0;
    }
	return a;
}

float elevationDiskRidge(vec2 p, vec3 c) {
    return c.z + ridgeNoise(p) - ridgeNoise(c.xy);
}

float elevationDiskPerlin(vec2 p, vec3 c) {
    return c.z + Terrain(p) - Terrain(c.xy);
}

float elevationLinePerlin(vec2 p, vec2 a, vec2 b, float deep) {
    float d = distanceLine(p, a, b);
    return deep + Noise(log(d));
}

// Implicit surface defining the terrain
// p : Point
float Implicit(in vec3 p)
{
	float h = Terrain(p.xy);
    float a = 1.0;
    
    vec3 cTemp = vec3(1200.0, 0.0, 600.);
    float alphaTemp = influenceDisk(p.xy, cTemp.xy, 1000.);
	float elevTemp = elevationDiskRidge(p.xy, cTemp);
    h = elevationBlending(h, elevTemp, a, alphaTemp);
    a = influenceBlending(a, alphaTemp);
    
    cTemp = vec3(600.0, -1600.0, 600.);
   	alphaTemp = influenceCircle(p.xy, cTemp.xy, 700.0, 800.0);
    elevTemp = elevationDiskRidge(p.xy, cTemp);
    h = elevationBlending(h, elevTemp, a, alphaTemp);
    a = influenceBlending(a, alphaTemp);
    
    
    cTemp = vec3(30.0, 0.0, 400.);
   	alphaTemp = influenceCircle(p.xy, cTemp.xy, 500.0, 400.0);
    elevTemp = elevationDiskRidge(p.xy, cTemp);
    h = elevationBlending(h, elevTemp, a, alphaTemp);
    a = influenceBlending(a, alphaTemp);
    
    vec2 x = vec2(30., 0.0);
    vec2 y = vec2(-800.0, 800.0);
    alphaTemp = influenceLine(p.xy, x, y, 400.0);
    elevTemp = elevationLinePerlin(p.xy, x, y, -320.);
    h = elevationBlending(h, elevTemp, a, alphaTemp);
    a = influenceBlending(a, alphaTemp);

    cTemp = vec3(-800.0, 1000.0, -400.);
    alphaTemp = influenceCircle(p.xy, cTemp.xy, 300., 1000.);
	elevTemp = elevationDiskPerlin(p.xy, cTemp);
    h = elevationBlending(h, elevTemp, a, alphaTemp);
    a = influenceBlending(a, alphaTemp);
    
    x = vec2(1400., 800.0);
    y = vec2(1400., 1400.0);
    cTemp = vec3(800.0, 1150.0, 400.);
    alphaTemp = influenceLine(p.xy, x, y, 1400.0);
    elevTemp = elevationDiskRidge(p.xy, cTemp);
    h = elevationBlending(h, elevTemp, a, alphaTemp);
    a = influenceBlending(a, alphaTemp);
    
    h *= a;
    return p.z - h;
}

// Sphere tracing
// ro, rd : Ray origin and direction
// t : Intersection depth
// i : Iteration count
bool Intersect(in vec3 ro, in vec3 rd, out float t,out int i)
{
    t = 0.0;
	for( i=0; i<Steps; i++ )
	{
        vec3 p = ro + t*rd;
		float h = Implicit(p);
        // 1 cm precision at 1 meter range, reduce precision as we get farther from eye
        if( abs(h)< Epsilon*sqrt(1.0+t) ) { return true; }
        if( t>view ) { return false; }
		// Empirical Lipschitz constant with level of detail (the further, the larger steps)
        t += max(Epsilon,h*sqrt(1.0+8.0*t/view)/K);
	}

	return false;
}

// Analysis of the scalar field --------------------------------------------------------------------------

// Calculate object normal
// p : point
vec3 Normal(in vec3 p )
{
  float eps = 0.01;
  vec3 n;
  float v = Implicit(p);
  n.x = Implicit( vec3(p.x+eps, p.y, p.z) ) - v;
  n.y = Implicit( vec3(p.x, p.y+eps, p.z) ) - v;
  n.z = Implicit( vec3(p.x, p.y, p.z+eps) ) - v;
  return normalize(n);
}

// Shading with number of steps
vec3 ShadeSteps(int n)
{
   float t=float(n)/(float(Steps-1));
   return 0.5+mix(vec3(0.05,0.05,0.5),vec3(0.65,0.39,0.65),t);
}

// Rendering 
// ro, rd : Ray origin and direction
// pip : Picture in picture boolean
vec4 Render( in vec3 ro, in vec3 rd, bool pip )
{
    // Light direction
    const vec3 light1 = normalize( vec3(-0.8,-0.3,0.4) );

	vec3 col;
    float t;
    int it;
    bool b=Intersect( ro, rd, t , it);
    if( b==false)
    {
        // sky		
        col = vec3(0.35,0.65,0.95) - rd.z*rd.z*2.5;
    }
	else
	{
        // mountains		
		vec3 p = ro + t*rd;
        vec3 n = Normal( p );

        vec3 ref = reflect( rd, n );
        float fre = clamp( 1.0+dot(rd,n), 0.0, 1.0 );
        vec3 hal = normalize(light1-rd);


        // Slope
        float s = sqrt(pow(n.x, 2.0) + pow(n.y, 2.0)) / n.z;

        // Biomes height map
        float deepSeaLevel = 30.0 * Noise(p / 100.0) - 500.0;
        float seaLevel = 10.0 * Noise(p / 100.0) - 400.0;
        float sandLevel = 10.0 * Noise(p / 100.0) - 380.0;
        float grassLevel = 30.0 * Noise(p / 100.0) - 300.0;
        float forestLevel = 100.0 * Noise(p / 100.0) - 200.0;
        float snowLevel = 100.0 * Noise(p / 100.0);

        // Biomes color map
        vec3 deepSeaColor = vec3(0.0, 0.0, 0.3);
        vec3 seaColor = vec3(0.0, 0.0, 0.8);
        vec3 sandColor = vec3(0.9, 0.8, 0.4);
        vec3 grassColor = vec3(0.22, 0.4, 0.0);
        vec3 forestColor = vec3(0.05, 0.2, 0.05);
        vec3 snowColor = vec3(0.8);

        if (p.z < deepSeaLevel) {
            col = deepSeaColor;
        } else if (p.z < seaLevel) {
            col = seaColor;
        } else if (p.z < sandLevel && s < 1.0) {
            col = sandColor;
        } else if (p.z < grassLevel && s < 1.7) {
            col = grassColor;
        } else if (p.z < forestLevel && s < 1.5 && n.x > 0.0) {
            col = forestColor;
        } else if (p.z < forestLevel + 100.0 && s < 2.0 && n.x < 0.0) {
            // More forests when facing south
            col = forestColor;
        } else if (p.z > snowLevel && (s < 2.5 && n.x > 0.0 || s < 1.0)) {
            // Less snow when facing south
            col = snowColor;
        }

        // Pseudo diffuse lighting
        if (p.z > seaLevel) {
            float dif = 0.5*(1.0+dot( light1, n ));
            dif*=dif;
            
            col += dif*vec3(0.25);
        }

		// Fog
        float fo = 1.0-exp(-pow(t/fog,1.5) );
        vec3 fco = 0.65*vec3(0.4,0.65,1.0);
        col = mix( col, 0.5*col+0.51*vec3(0.35,0.65,0.95), fo );

	}
    
    // Shading for iterations
    if (pip==true)
    {
    	return vec4(ShadeSteps(it),1.0);
    }
    else
    {
    // Gamma with square root
       return vec4( sqrt(col), t );
    }

}

mat3 moveCamera(float time, out vec3 ro)
{
	// Origin
    ro =  vec3(2000.0*cos(iMouse.x*0.01),2000.0*sin(iMouse.x*0.01),1000.0) ;
	
    // Target
    vec3 ta = vec3(0.0,0.0,500.0);
    
	vec3 cw = normalize(ta-ro);
	vec3 cp = vec3(0.0, 0.0,1.0);
	vec3 cu = normalize( cross(cw,cp) );
	vec3 cv = normalize( cross(cu,cw) );
    return mat3( cu, cv, cw );
}

vec2 RayDirection(in vec2 pixel, out bool pip)
{
    // Pixel coordinates
    vec2 p = (-iResolution.xy + 2.0*pixel)/iResolution.y;
   if (pip==true)
   {    
    const float fraction=1.0/4.0;
    // Picture in picture
    if ((pixel.x<iResolution.x*fraction) && (pixel.y<iResolution.y*fraction))
    {
        p=(-iResolution.xy*fraction + 2.0*pixel)/(iResolution.y*fraction);
        pip=true;
    }
       else
       {
           pip=false;
       }
   }
   return p;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
   // Time as function of the position of the mouse
    float time = iTime*0.25 + 4.0*iMouse.x/iResolution.x;

    // Camera transform
    vec3 ro; 
    mat3 cam = moveCamera( time, ro);   

    // Picture in picture
    bool pip=true;
    
    // Pixel coordinates
    vec2 p = RayDirection(fragCoord, pip);
   
    // Camera ray    
    vec3 rd = cam * normalize(vec3(p,3.0));
    
    // Render
    vec4 res = Render( ro, rd, pip );
     
    fragColor = vec4( res.xyz, 0.0 );
    
}
