// Texturing
// Eric Galin

// Texturing and noise ---------------------------------------------------------

// Hashing function
// Returns a random number in [-1,1]
// p : Vector in space
float Hash(in vec3 p)  
{
    p  = fract( p*0.3199+0.152 );
	p *= 17.0;
    return fract( p.x*p.y*p.z*(p.x+p.y+p.z) );
}

// Procedural value noise with cubic interpolation
// x : Point 
float Noise(in vec3 p)
{
    vec3 i = floor(p);
    vec3 f = fract(p);
  
    f = f*f*(3.0-2.0*f);
    // Could use quintic interpolation instead of cubic
    // f = f*f*f*(f*(f*6.0-15.0)+10.0);

    return mix(mix(mix( Hash(i+vec3(0,0,0)), 
                        Hash(i+vec3(1,0,0)),f.x),
                   mix( Hash(i+vec3(0,1,0)), 
                        Hash(i+vec3(1,1,0)),f.x),f.y),
               mix(mix( Hash(i+vec3(0,0,1)), 
                        Hash(i+vec3(1,0,1)),f.x),
                   mix( Hash(i+vec3(0,1,1)), 
                        Hash(i+vec3(1,1,1)),f.x),f.y),f.z);
}

// Hashing function
// Returns a random number in [-1,1]
// p : Vector in the plane
float Hash(in vec2 p)  
{
    p  = fract( p*0.3199+0.152 );
	p *= 17.0;
    return fract( p.x*p.y*(p.x+p.y) );
}


// Procedural value noise with cubic interpolation
// x : Point 
float Noise(in vec2 p)
{
    vec2 i = floor(p);
    vec2 f = fract(p);
  
    f = f*f*(3.0-2.0*f);
    // Could use quintic interpolation instead of cubic
    // f = f*f*f*(f*(f*6.0-15.0)+10.0);

    return mix(mix( Hash(i+vec2(0,0)), 
                        Hash(i+vec2(1,0)),f.x),
                   mix( Hash(i+vec2(0,1)), 
                        Hash(i+vec2(1,1)),f.x),f.y);
}

float influence(float d, float r) {
    if (d < r)
    	return pow(1.0 - pow(d, 2.0) / pow(r, 2.0), 3.0);
    else
        return 0.0;
}

float distanceLine(vec2 p, vec2 a, vec2 b) {
    float distance = 0.;
    vec2 u = normalize(b - a);
    float ah = dot(p - a, u);
    if (ah < 0.) {
    	distance = length(p - a);	    
    } else if (ah > length(b - a)) {
        distance = length(p - b);
    } else {
   		distance = sqrt(pow(length(p - a), 2.) - pow(ah, 2.));
    }
    
    return distance;
}

float influenceLine(vec2 p, vec2 a, vec2 b, float r) {
    float d = distanceLine(p, a, b);
    return influence(d, r);
}

float distanceCircle(vec2 p, vec2 c, float r) {
    return abs(length(p - c) - r);
}

float influenceCircle(vec2 p, vec2 c, float R, float r) {
    float d = distanceCircle(p, c, R);
    return influence(d, r);
}

float distanceCappedTorus(in vec2 p, in vec2 c, float r, in float ra, in float rb)
{
  	vec2 x = vec2(c.x + r * cos(ra), c.y + r * sin(ra));
    vec2 y = vec2(c.x + r * cos(rb), c.y + r * sin(rb));
    
    float temp = min(length(p - x), length(p - y));
    return min(temp, distanceCircle(p, c, r));
    
}

float influenceCappedTorus(vec2 p, vec2 c, float R, float ra, float rb, float r) {
    float d = distanceCappedTorus(p, c, R, ra, rb);
    return influence(d, r);
}

float influenceDisk(vec2 p, vec2 c, float r) {
    float d = length(p - c);
    return influence(d, r);
}

float elevationBlending(float h1, float h2, float alpha1, float alpha2) {
    return (alpha1 * h1 + alpha2 * h2) / (alpha1 + alpha2);
}

float influenceBlending(float alpha1, float alpha2) {
    return alpha1 + alpha2;
}

float elevationReplacing(float h1, float h2, float alpha1, float alpha2) {
    return (1. - alpha1) * h1 + alpha1 * h2;
}

float influenceReplacing(float alpha1) {
    return alpha1;
}

// Procedural value noise with cubic interpolation
// x : Point 
float Noise(in float p)
{
    float i = floor(p);
    float f = fract(p);
  
    f = f*f*(3.0-2.0*f);
    // Could use quintic interpolation instead of cubic
    // f = f*f*f*(f*(f*6.0-15.0)+10.0);

    return mix(Hash(i+vec2(0,0)), Hash(i+vec2(1,0)), f);
}
