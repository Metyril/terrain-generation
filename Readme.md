# TP1 - Modélisation de monde virtuel - ShaderToy

Sujet : Modélisation de terrain à l'aide de bruit et texturage de celui-ci.

Membres:
MARTIN Clément 11807010
BOROWY--VANACKERE Malo 11611321

Lien Shadertoy : https://www.shadertoy.com/view/wdcBzj

# Génération du terrain

### Bruits

Nous avons réalisé notre terrain à l'aide de deux bruits différents. Le premier se rapproche du bruit de perlin (plus doux) et nous sert pour la modélisation du terrain de base ainsi que pour les lacs ou rivières. Le second se rapproche du ridge noise (avec plus de pics) et nous sert pour réaliser les reliefs montagneux.

### Primitives

Pour ce qui est des primitives, nous avons défini une primitive point pour les montagnes, une primitive cercle pour faire des volcans ou des lacs et la primitives ligne qui peut servir pour les montagnes et les rivières.
Chacune de ses primitives à sa fonction d'influence associée et nous avons également créé une fonction d'élévation pour les reliefs montagneux et une pour les reliefs comme la rivière.

### Opérateurs

Nous avons codé l'opérateur de blend pour nos reliefs : un blend d'influence et un blend d'élévation.
Nous avons aussi essayé de réaliser l'opérateur de substitution mais que nous n'avons pas réussi à exploiter.

### Textures

Nous avons défini des couches par altitude (bruitée) : eau profonde, eau, sable, herbe, forêt, montagne, neige.
Il y a moins de neiges et plus de forêts dans les parties orientées vers le sud.
Nous avons retiré la neige, le sable, l'herbe et la forêt dans les parties les plus pentues.

### Captures d'écrans

![Mountains](./screenshots/terrain1.png)

![Lakes](./screenshots/terrain2.png)